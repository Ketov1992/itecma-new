$(document).ready(function () {

    if ($('.itecma__map').length > 0) {
        //ymaps.ready(initMap);

        ymaps.ready(init);

        function init() {
            var center1 = [55.703588, 37.523185];
            var myMap = new ymaps.Map('map', {
                center: center1,
                zoom: 16,
                controls: []
            });

            myMap.behaviors.disable('scrollZoom');

            var myPlacemark = new ymaps.Placemark(center1, {
                hintContent: 'Москва, Ленинские Горы, дом 1, строение 11'
            });

            myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
                hintContent: 'Большой Казачий переулок, 8'
            }, {
                iconLayout: 'default#image',
                iconImageHref: '/uploads/images/map-pin.png',
                //iconImageHref: 'assets/img/map-pin.png',
                iconImageSize: [43, 64],
                iconImageOffset: [-3, -42]
            });

            var templateZoomPlus =
                    '<div class="itecma__map-map-zoom-plus">' +
                    "{{ data.content }}" +
                    "</div>";

            var templateZoomMinus =
                    '<div class="itecma__map-map-zoom-minus">' +
                    "{{ data.content }}" +
                    "</div>";

            var zoomPlus = new ymaps.control.Button({
                data: {
                    content: ""
                },
                options: {
                    layout:
                            ymaps.templateLayoutFactory.createClass(
                                    templateZoomPlus
                                    ),
                    maxWidth: 100
                }});

            var zoomMinus = new ymaps.control.Button({
                data: {
                    content: ""
                },
                options: {
                    layout:
                            ymaps.templateLayoutFactory.createClass(
                                    templateZoomMinus
                                    ),
                    maxWidth: 100
                }});


            zoomPlus.events.add('click', function () {
                var zoom = myMap.getZoom();
                myMap.setZoom(zoom + 1)
            });

            zoomMinus.events.add('click', function () {
                var zoom = myMap.getZoom();
                myMap.setZoom(zoom - 1)
            });

            myMap.controls.add(zoomPlus, {});
            myMap.controls.add(zoomMinus, {});
            myMap.geoObjects.add(myPlacemark);

        }
    }

});