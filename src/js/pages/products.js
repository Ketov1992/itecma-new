$(document).ready(function () {
    var sudoSlider = $(".services__container-slider").sudoSlider({
        slideCount: 1,
        moveCount: 1,
        startSlide: 1,
        continuous: true,
        speed: 300,
        controlsAttr: 'class="product-slider__controls"',
        prevHtml: '<div class="services__slider-nav services__slider-nav--prev"><div class="services__slider-nav-img services__slider-nav-img--prev services__slider-nav-img--prev-normal"></div><div class="services__slider-nav-img services__slider-nav-img--prev services__slider-nav-img--prev-active"></div></div>',
        nextHtml: '<div class="services__slider-nav services__slider-nav--next"><div class="services__slider-nav-img services__slider-nav-img--next services__slider-nav-img--next-normal"></div><div class="services__slider-nav-img services__slider-nav-img--next services__slider-nav-img--next-active"></div></div>'
    });
});