$(document).ready(function () {
    //Слайдер на главной
    var sudoSlider = $(".index-slider").sudoSlider({
        pause: 3000,
        auto: true,
        slideCount: 1,
        continuous: true,
        speed: 300
    });

    $('.prevBtn').html('<div class="index-slider__arrow index-slider__arrow--left"><div class="index-slider__arrow-image index-slider__arrow-image--left-normal"></div><div class="index-slider__arrow-image index-slider__arrow-image--left-active"></div></div>');
    $('.nextBtn').html('<div class="index-slider__arrow index-slider__arrow--right"><div class="index-slider__arrow-image index-slider__arrow-image--right-normal"></div><div class="index-slider__arrow-image index-slider__arrow-image--right-active"></div></div>');

});