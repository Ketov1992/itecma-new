$(document).ready(function () {
    //$(".fancybox").fancybox({minWidth: 450});

    $(".fancybox").fancybox({
        afterLoad: function () {
            this.title = '<div style="background-image: url(' + this.element.data('bg') + ');"' + 'class="fancybox-image"></div>' +
                    '<div class="fancybox-text"><div class="fancybox-h1">' + this.title + '</div><div class="fancybox-post">' + this.element.data('post') + '</div><div>' + this.element.data('text') + '</div></div>' +
                    '<div class="clear"></div>';
        },
        helpers: {
            title: {
                type: 'inside'
            },
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        },
        beforeShow: function () {
            $.extend(this, {
                tpl: {
                    closeBtn: '<div class="popup-description__close-container"><div class="popup-description__close-img popup-description__close-img--normal"></div><div class="popup-description__close-img popup-description__close-img--hover"></div></div>',
                }
            }); // extend
        },
        arrows: false
    });



//Одинаковая высота колонок
    $(function () {
        $('.team__employee').matchHeight({
            byRow: true,
            property: 'height',
            target: null,
            remove: false
        });
    });
});