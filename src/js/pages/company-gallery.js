$(document).ready(function () {
    //$(".fancybox").fancybox({minWidth: 450});

    $(".fancybox-company-gallery").fancybox({
        //'openEffect': 'elastic',
        //nextMethod: 'zoomIn',
        prevEffect : 'fade',
        //nextMethod: 'slideIn',
        prevSpeed : 400,
        afterLoad: function () {
            this.title = '<div style="background-image: url(' + this.element.data('bg') + ');"' + 'class="fancybox-image-company-gallery"></div>' +
                    '<div class="fancybox-text">' + this.element.data('text') + '</div>';
        },
        helpers: {
            title: {
                type: 'inside'
            },
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        },
        beforeShow: function () {
            $.extend(this, {
                tpl: {
                    closeBtn: '<div class="popup-description__close-container"><div class="popup-description__close-img popup-description__close-img--normal"></div><div class="popup-description__close-img popup-description__close-img--hover"></div></div>',
                    next: '<div class="product-slider__nav product-slider__nav--zindex product-slider__nav--next"><div class="product-slider__nav-img product-slider__nav-img--next product-slider__nav-img--next-normal"></div><div class="product-slider__nav-img product-slider__nav-img--next product-slider__nav-img--next-active"></div></div>',
                    prev: '<div class="product-slider__nav product-slider__nav--zindex product-slider__nav--prev"><div class="product-slider__nav-img product-slider__nav-img--prev product-slider__nav-img--prev-normal"></div><div class="product-slider__nav-img product-slider__nav-img--prev product-slider__nav-img--prev-active"></div></div>'
                }
            }); // extend
        }

    });

});