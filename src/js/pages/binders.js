$(document).ready(function () {

    function bindersHeight() {
        $('.binders__col').each(function () {
            var el = $(this);
            el.css({'height': el.parents('.binders__table-row').height()});
        });
    }
    
    bindersHeight();
    $(window).resize(function(){
        bindersHeight();
    });

});