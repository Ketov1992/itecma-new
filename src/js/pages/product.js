$(document).ready(function () {
    var sudoSlider = $(".product-slider__slider").sudoSlider({
        slideCount: 1,
        moveCount: 1,
        startSlide: 1,
        continuous: true,
        speed: 300,
        controlsAttr: 'class="product-slider__controls"',
        prevHtml: '<div class="product-slider__nav product-slider__nav--prev"><div class="product-slider__nav-img product-slider__nav-img--prev product-slider__nav-img--prev-normal"></div><div class="product-slider__nav-img product-slider__nav-img--prev product-slider__nav-img--prev-active"></div></div>',
        nextHtml: '<div class="product-slider__nav product-slider__nav--next"><div class="product-slider__nav-img product-slider__nav-img--next product-slider__nav-img--next-normal"></div><div class="product-slider__nav-img product-slider__nav-img--next product-slider__nav-img--next-active"></div></div>'
    });
    //$('.prevBtn').html('<div class="product-slider__nav product-slider__nav--prev"><img src="/assets/img/slider-arrow-small-left.svg" /></div>');
    //$('.nextBtn').html('<--');

    $('.product__anchors-anchor').on('click', function () {
        var el = $(this);
        $('.product__anchors-anchor--selected').removeClass('product__anchors-anchor--selected');
        el.addClass('product__anchors-anchor--selected');
    });

    function scrollTo(element, anchor) {
        $(element).click(function () {
            var destination = $(anchor).offset().top;
            jQuery("html:not(:animated),body:not(:animated)").animate({scrollTop: destination}, 500);
            return false;
        });
    }

    scrollTo('#product__anchors-anchor--description', '#product__description-block');
    scrollTo('#product__anchors-anchor--types', '#product__types');
    scrollTo('#product__anchors-anchor--area', '#product__area');

    $('.product__read-more-button').on('click', function () {
        var el = $(this);
        if (el.hasClass('product__read-more-button--active')) {
            el.removeClass('product__read-more-button--active');
            $('.product__description-hidden').slideUp(300);

        } else {
            el.addClass('product__read-more-button--active');
            $('.product__description-hidden').slideDown(300);
        }

    });
});