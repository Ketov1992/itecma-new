$(document).ready(function () {
    $('.history__link').on('click', function () {
        var el = $(this);
        var show = el.siblings('.history__text-container').children('.history__text-hidden');
        if (show.hasClass('history__text-hidden--active')) {
            show.removeClass('history__text-hidden--active');
            show.slideUp(200);
            //jQuery.fn.matchHeight._update();
        } else {
            show.addClass('history__text-hidden--active');
            show.slideDown(300);
            //jQuery.fn.matchHeight._update();
        }
        return false;
    });
});