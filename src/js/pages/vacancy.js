$(document).ready(function () {
    $('.green-link--vacancy').on('click', function () {
        var el = $(this);
        var show = el.parent('.vacancy__bottom').siblings('.vacancy__hidden');
        if (show.hasClass('vacancy__hidden--active')) {
            show.removeClass('vacancy__hidden--active');
            show.slideUp(200);
        } else {
            show.addClass('vacancy__hidden--active');
            show.slideDown(300);
        }
        return false;
    });
});