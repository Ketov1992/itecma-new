'use strict';

$(document).ready(function () {
    //Работа с верхним меню
    window.onscroll = function () {
        var scrolled = window.pageYOffset || document.documentElement.scrollTop;
        if (scrolled !== 0) {
            $('.header').addClass('header--active');
            $('.header__menu').addClass('header__menu--active');
            $('.header__menu-container').addClass('header__menu-container--active');
            $('.header__guide').addClass('header__guide--active');
        } else {
            $('.header').removeClass('header--active');
            $('.header__menu').removeClass('header__menu--active');
            $('.header__menu-container').addClass('header__menu-container--active');
            $('.header__guide').removeClass('header__guide--active');
            $('.header__guide').addClass('header__guide--opacity');
            setTimeout(function () {
                $('.header__guide').removeClass('header__guide--opacity');
            }, 500);
        }
    };

    $('.header__search').on('click', function () {
        $('.header__search').addClass('header__search--active');
        $('.header__search-input').addClass('header__search-input--active');
        $('.header__search-close').addClass('header__search-close--active');
    });

    $('.header__search-close').on('click', function () {
        $('.header__search').removeClass('header__search--active');
        $('.header__search-input').removeClass('header__search-input--active');
        $(this).removeClass('header__search-close--active');
    });

    $('.header__search-input').on('focus', function () {
        var el = $(this);
        el.addClass('header__search-input--focus');
        $('.header__search-close').addClass('header__search-close--focus');
        $('.header__search').addClass('header__search--focus');
    });

    $('.header__search-input').on('focusout', function () {
        var el = $(this);
        el.removeClass('header__search-input--focus');
        $('.header__search-close').removeClass('header__search-close--focus');
        $('.header__search').removeClass('header__search--focus');
    });
});