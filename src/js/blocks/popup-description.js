$(document).ready(function () {
    $('.product__slider-link').on('click', function () {
        var top = $(window).scrollTop() + 40;
        var text = $(this).attr('data-text');
        $('.popup-description__content').html(text);
        $('.popup-description').css({'top': top});
        $('.black-bg').fadeIn(400);
        setTimeout(function () {
            $('.popup-description').fadeIn(400);
        }, 480);

        return false;
    });

    $('.popup-description__close-container').on('click', function () {
        $('.popup-description').fadeOut(400);
        setTimeout(function () {
            $('.black-bg').fadeOut(400);
        }, 480);
    });
    
    $('.green-button__button--popup-description').on('click', function(){
        $('.popup-description').fadeOut(400);
        $('.consultation').addClass('consultation--popup-description');
    });
});