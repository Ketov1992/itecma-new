$(document).ready(function () {
    $('.vacancy__button').on('click', function () {
        //$("html:not(:animated),body:not(:animated)").animate({scrollTop: 0}, 350);
        var top = $(window).scrollTop()+40;
        $('.career-popup').css({'top':top});
        $('.black-bg').fadeIn(400);
        setTimeout(function () {
            $('.career-popup').fadeIn(400);
        }, 480);
    });

    $('.career-popup__close').on('click', function () {
        $('.career-popup').fadeOut(400);
        setTimeout(function () {
            $('.black-bg').fadeOut(400);
        }, 480);
    });
});