$(document).ready(function () {
    //Работа с селектом в попапе консультации
    $('.consultation__input--select').on('click', function () {
        var e = $(this);
        if (e.hasClass('consultation__input--select--active')) {
            e.removeClass('consultation__input--select--active');
            e.siblings('.consultation__select-variants').hide();
            e.siblings('.consultation__select-arrow').removeClass('consultation__select-arrow--active');
        } else {
            e.addClass('consultation__input--select--active');
            e.siblings('.consultation__select-variants').show();
            e.siblings('.consultation__select-arrow').addClass('consultation__select-arrow--active');
        }
    });

    $('.consultation__select-variants-li').on('click', function () {
        var e = $(this);
        var el = e.parents('.consultation__select-variants').siblings('.consultation__input--select--active');
        var val = e.text();
        el.removeClass('consultation__input--select--active');
        el.siblings('.consultation__select-variants').hide();
        el.siblings('.consultation__select-arrow').removeClass('consultation__select-arrow--active');
        el.val(val);
    });

    $('.consultation__select-arrow').on('click', function () {
        var e = $(this);
        var el = e.siblings('.consultation__input');
        if (e.hasClass('consultation__select-arrow--active')) {
            el.removeClass('consultation__input--select--active');
            el.siblings('.consultation__select-variants').hide();
            el.siblings('.consultation__select-arrow').removeClass('consultation__select-arrow--active');
        } else {
            el.addClass('consultation__input--select--active');
            el.siblings('.consultation__select-variants').show();
            el.siblings('.consultation__select-arrow').addClass('consultation__select-arrow--active');
        }
    });


//Работа с попапом консультации
    $('.popup-consultation-button').on('click', function () {
        //$("html:not(:animated),body:not(:animated)").animate({scrollTop: 0}, 350);
        var top = $(window).scrollTop() + 40;
        $('.consultation').css({'top': top});
        $('.black-bg').fadeIn(400);
        setTimeout(function () {
            $('.consultation').fadeIn(400);
        }, 480);
    });

    $('.consultation__close').on('click', function () {
        if ($('.consultation').hasClass('consultation--popup-description')) {
            $('.consultation').removeClass('consultation--popup-description');
            $('.consultation').fadeOut(400);   
            setTimeout(function(){
                $('.popup-description').fadeIn(400);
            }, 480);
        } else {
            $('.consultation').fadeOut(400);
            setTimeout(function () {
                $('.black-bg').fadeOut(400);
            }, 480);
        }
    });
});