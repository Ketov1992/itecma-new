'use strict'


function isMobile() {
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        return true
    } else {
        return false
    }
}

$(document).ready(function () {

    //Работа с правым фиксированным меню
    /*$('.header__menu').on('click', function () {
     var el = $(this);
     if (el.hasClass('right-menu-fixed__close')) {
     el.removeClass('right-menu-fixed__close');
     el.children('.header__menu-text').text('Меню');
     $('.right-menu-fixed__menu-container').css({'width': '0'});
     setTimeout(function () {
     $('.right-menu-fixed').fadeOut(400);
     $('.right-menu-fixed__link1, .right-menu-fixed__link1--active').css({'padding': '16px 0 16px 230px'});
     }, 1000);
     $('.header-logo').css({'z-index': '3'});
     }else {
     el.css({'opacity': '0'});
     $('.right-menu-fixed').fadeIn(400);
     setTimeout(function () {
     el.addClass('right-menu-fixed__close');
     el.children('.header__menu-text').text('Закрыть');
     $('.header-logo').css({'z-index': '2'});
     $('.right-menu-fixed__menu-container').css({'width': '37.5%'});
     $('.right-menu-fixed__link1, .right-menu-fixed__link1--active').css({'padding': '16px 0 16px 107px'});
     }, 500);
     setTimeout(function () {
     el.animate({opacity: "1"}, 250);
     }, 1600);
     }
     });*/
    
    function marginContent(){
        var width = $(window).width();
        if(width > 1242){
            var margin = (width - 1242) / 2;
            //alert(margin); 
            $('.itecma__main-content').css({'margin-left':margin});
        }
        else{
            $('.itecma__main-content').css({'margin-left':0});
        }
    }
    marginContent();
    $(window).resize(function(){
        marginContent();
    });

    $('.header__menu').on('click', function () {
        //$('.header-logo').css({'z-index': '2'});
        //$('.right-menu-fixed').stop(true, true).fadeIn(400);
        $('.black-bg, .right-menu-fixed').stop(true, true).fadeIn(400);
        setTimeout(function () {
            $('.right-menu-fixed__menu-container').stop(true, true).css({'width': '37.5%'});
            $('.right-menu-fixed__link1, .right-menu-fixed__link1--active').stop(true, true).css({'padding': '16px 0 16px 107px'});
        }, 500);
    });

    $('.right-menu-fixed').click(function (event) {
        if ($(event.target).closest(".right-menu-fixed__menu-container").length) {
            return;
        } else {
            closeMenu();
        }
    });

    $('.right-menu-fixed__close').on('click', function () {
        closeMenu();
    });

    function closeMenu() {
        $('.right-menu-fixed__menu-container').stop(true, true).css({'width': '0'});
        setTimeout(function () {
            $('.black-bg').stop(true, true).fadeOut(350);
            $('.right-menu-fixed').stop(true, true).fadeOut(550);
            //$('.right-menu-fixed').stop(true, true).css({'opacity':'0'});
            $('.right-menu-fixed__link1, .right-menu-fixed__link1--active').stop(true, true).css({'padding': '16px 0 16px 230px'});
            //$('.header-logo').css({'z-index': '3'});
        }, 1000);
    }

    //Работа с селектами
    $('.select').on('click', function () {
        var e = $(this);
        if (e.hasClass('select--active')) {
            e.removeClass('select--active');
            e.children('.select__container').removeClass('select__container--active');
            e.children('.select__select-arrow').removeClass('select__select-arrow--active');            
        } else {
            e.addClass('select--active');
            e.children('.select__container').addClass('select__container--active');
            e.children('.select__select-arrow').addClass('select__select-arrow--active');
            var element = e.children('.select__container').children('.select__select-categories').jScrollPane({/* ...settings... */});
            var api = element.data('jsp');
        }
    });

    $('.select__select-category').on('click', function () {
        var e = $(this);
        var el = e.closest('.select');
        var val = e.children('.select__select-category-span').text();
        el.children('.select__input').val(val).trigger('change');
        el.removeClass('.select--active');
        //el.siblings('.consultation__select-variants').hide();
        //el.siblings('.consultation__select-arrow').removeClass('consultation__select-arrow--active');
    });



    /*(function () {
     var el;
     $('.right-menu-fixed__ul1-li').hover(
     function () {
     el = $(this);
     el.children('.right-menu-fixed__ul2').stop(true, true).slideDown(250);
     },
     function () {
     el.children('.right-menu-fixed__ul2').stop(true, true).slideUp(250);
     });
     }());*/

    $('.right-menu-fixed__hidden').on('click', function () {
        var el = $(this);
        if (el.hasClass('right-menu-fixed__hidden--active')) {
            el.children('.right-menu-fixed__ul2').stop(true, true).slideUp(250);
            el.removeClass('right-menu-fixed__hidden--active');
        } else {
            el.children('.right-menu-fixed__ul2').stop(true, true).slideDown(250);
            el.addClass('right-menu-fixed__hidden--active');
        }

        //return false;
    });


    window.onload = function () {
        $('#articles').isotope({
            itemSelector: '.articles__article',
            layoutMode: 'masonry'
        });

        $(function () {
            /*$('.itecma__column').matchHeight({
             byRow: true,
             property: 'height',
             target: null,
             remove: false
             });*/
        });
    };


    //Фиксированное правое меню
    $('.right-menu__ul').simpleScrollFollow();

    $('.footer__top').on('click', function () {
        $("html:not(:animated),body:not(:animated)").animate({scrollTop: 0}, 350);
    });

    //Работа с SVG
    /*$('.index-services__link--1').hover(
     function () {
     var a = document.getElementById("object-1");
     var svgDoc = a.contentDocument;
     var svg = svgDoc.getElementById("svg-1");
     svg.setAttribute('class', 'svg-1--active');
     },
     function () {
     var a = document.getElementById("object-1");
     var svgDoc = a.contentDocument;
     var svg = svgDoc.getElementById("svg-1");
     svg.setAttribute('class', '');
     });
     */
    //alert(1);
    //$('#svg-1').addClass('svg-1--active');
    $('.index-services__link--1').hover(
            function () {
                $('#svg-1').addClass('svg-1--active');
            },
            function () {
                $('#svg-1').removeClass('svg-1--active');
            });

    $('.index-services__link--2').hover(
            function () {
                $('#svg-2').addClass('svg-1--active');
            },
            function () {
                $('#svg-2').removeClass('svg-1--active');
            });
    //function svgHover() {
    //var a = document.getElementById("svg-1").contentDocument;
    //a.setAttribute('class', 'svg-1--active');
    //var svgDoc = a;
    // var svg = svgDoc.getElementById("svg-1");
    //svg.setAttribute('class', 'svg-1--active');
    //}

    //svgHover();


    /*function svgHover(o, svg) {
     var a = document.getElementById(o);
     var svgDoc = a.contentDocument;
     var svg = svgDoc.getElementById(svg);
     svg.setAttribute('class', 'svg-1--active');
     }
     
     function svgHoverOut(o, svg) {
     var a = document.getElementById(o);
     var svgDoc = a.contentDocument;
     var svg = svgDoc.getElementById(svg);
     svg.setAttribute('class', '');
     }
     
     window.onload = function () {
     var a = document.getElementById("object-1");
     var checkDoc = a.contentDocument;
     var svg = checkDoc.getElementById('svg-1');
     svg.setAttribute('class', 'svg-1--active');
     };
     
     
     
     $('.index-services__link--1').hover(
     function () {
     svgHover("object-1", "svg-1");
     },
     function () {
     svgHoverOut("object-1", "svg-1");
     });
     
     $('.index-services__link--2').hover(
     function () {
     svgHover("object-2", "svg-2");
     },
     function () {
     svgHoverOut("object-2", "svg-2");
     });*/


    var body = $('body'),
            timer,
            windowHeight = $(window).height(),
            windowWidth = $(window).width()

//Задаем размеры блокам
    var setHeight = function () {

    }
    setHeight();

    $(window).bind('resize', function () {
        windowHeight = window.innerHeight;
        windowWidth = window.innerWidth;
        setHeight();
    });

    $(window).bind('scroll', function () {

        //отключаем ховер при скроле
        clearTimeout(timer);
        if (!body.hasClass('disable-scroll-hover')) {
            body.addClass('disable-scroll-hover')
        }
        timer = setTimeout(function () {
            body.removeClass('disable-scroll-hover')
        }, 200);

    });
});