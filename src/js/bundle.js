'use strict';

//
// Подключаем модули
//

// Bootstrap
//require("../../bower_components/bootstrap-sass/assets/javascripts/bootstrap/transition.js")
//require("../../bower_components/bootstrap-sass/assets/javascripts/bootstrap/affix.js")
//require("../../bower_components/bootstrap-sass/assets/javascripts/bootstrap/alert.js")
//require("../../bower_components/bootstrap-sass/assets/javascripts/bootstrap/button.js")
//require("../../bower_components/bootstrap-sass/assets/javascripts/bootstrap/carousel.js")
//require("../../bower_components/bootstrap-sass/assets/javascripts/bootstrap/collapse.js")
//require("../../bower_components/bootstrap-sass/assets/javascripts/bootstrap/dropdown.js")
//require("../../bower_components/bootstrap-sass/assets/javascripts/bootstrap/modal.js")
//require("../../bower_components/bootstrap-sass/assets/javascripts/bootstrap/scrollspy.js")
//require("../../bower_components/bootstrap-sass/assets/javascripts/bootstrap/tab.js")
//require("../../bower_components/bootstrap-sass/assets/javascripts/bootstrap/tooltip.js")
//require("../../bower_components/bootstrap-sass/assets/javascripts/bootstrap/popover.js")

// Owl-Carousel
import "../../bower_components/owl.carousel/dist/owl.carousel.js";

// Библиотеки
import "script!./libs/jquery.matchHeight-min.js";
import "script!./libs/jquery.sudoSlider.min.js";
import "script!./libs/jquery.simple-scroll-follow.min.js";
import "script!./libs/jquery.mousewheel.js";
import "script!./libs/jquery.jscrollpane.min.js";
import "script!./libs/jquery.fancybox.pack.js"
import "script!./libs/isotope.pkgd.min.js";

// Собственный js
import "./main";
import "./blocks/guide";
import "./blocks/header";
import "./blocks/popup-consultation";
import "./blocks/popup-description";
import "./blocks/popup-vacancy";
//import "./blocks/footer";

import "./pages/binders";
import "./pages/card";
import "./pages/company-gallery";
import "./pages/downloads";
import "./pages/history";
import "./pages/index";
import "./pages/product";
import "./pages/products";
import "./pages/services";
import "./pages/team";
import "./pages/contacts";
import "./pages/vacancy";
